'use strict';
angular.module('app.core', [])
// Set global services. Shouldn't set too many. These services are defined in app.services.js
.controller('appCtrl',function($scope,$rootScope,metaServices,baseServices){
  	$rootScope.metaservice = metaServices;
  	$rootScope._base = baseServices;
});