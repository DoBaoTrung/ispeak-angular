angular.module('app.core')
.controller('footerController', function($scope, baseServices, $http, $interval){

    $http.get(baseServices.api_url + 'options')
    .then(function(response) {
        $scope.option = response.data;
        // console.log(response.data);
    });

    $http.get(baseServices.api_url + 'footer_tags')
    .then(function(response) {
        $scope.footer_tags = response.data;
    });

    $http.get(baseServices.api_url + 'popup_banner')
    .then(function(response) {
        $scope.popup_banner = response.data;
    });

    $scope.today = new Date();

    $scope.popup = function(){
    	$interval(function(){
    		$('.mypopup').popup({
			  	transition: 'all 0.3s'
			});
	    	$('#home_popup_banner').popup('show');
    	}, 4000, 1);
    }

})
