/**
*  Module
*
* Description
*/
angular.module('app.core')
.controller('sliderController', function($scope, baseServices, $http){
	$http.get(baseServices.api_url + 'home_slides')
    .then(function(response) {
        $scope.slides = response.data;
    });

    $scope.$on('ngRepeatFinished', function(ngRepeatFinishedEvent) {
	    $('#home-slider .bxslider').bxSlider({
	        responsive: true,
	        adaptiveHeight: true
	    });
	});

	$scope.api_host = baseServices.api_host;

})