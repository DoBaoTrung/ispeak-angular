angular.module("app.core")
.directive("loginForm", function() {
  	return {
    	restrict: 'EA',
    	templateUrl: 'app/shared/loginform/loginform.html',
    	scope: true,
    	controller: 'loginFormController'
  	}
});