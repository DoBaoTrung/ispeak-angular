/**
 * - Trung - NOTE FROM AUTHOR: baseServices and metaServices are put into global scope 
 * as global services as they're reuse many places throughout many controllers. 
 * See details at app.core.js.
 */

angular.module('app.services', [])
.service('baseServices', function(){
    this.app_url  = 'ispeakng/app/';
    this.assets_url  = 'ispeakng/assets/';

    // Sở dĩ sinh ra cái dở hơi này là vì api bên server trả link ảnh dạng relative...
    this.api_host  = 'http://localhost/ispeakdev/';
    this.api_url  = this.api_host + 'api/home/';

    this.numberToArray = function(x){
    	return new Array(x);
    }
})
.service('metaServices', function() {
   	var title = '(Demo) iSpeak bản AngularJS';
   	var metaDescription = 'iSpeak bản AngularJS';
  	var metaKeywords = 'học tiếng anh';
   	return {
      	set: function(newTitle, newMetaDescription, newKeywords) {
          	metaKeywords = newKeywords;
          	metaDescription = newMetaDescription;
          	title = newTitle; 
      	},
      	getTitle: function(){ return title; },
      	getDescription: function() { return metaDescription; },
      	getKeywords: function() { return metaKeywords; }
   	}
});


