angular.module("app",[
	'ngRoute',
	'ngSanitize',
	'app.routes',
	'app.config',
	'app.services',
	'app.core',
])

