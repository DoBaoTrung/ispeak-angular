angular.module("app.routes",['ngRoute'])
.config(function($routeProvider,$locationProvider) {
	$locationProvider.html5Mode({
	    enabled: true,
	    requireBase: false
	});

    $routeProvider
    .otherwise({
    	templateUrl : "app/component/home/home.html",
    });
    
});