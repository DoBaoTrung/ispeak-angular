angular.module('app.core')
.controller('homeController', function($scope, baseServices){
	$scope.numberToArray = baseServices.numberToArray;
})
.controller('testimonialController', function($scope, baseServices , $http){
	$http.get(baseServices.api_url + 'testimonials').then(function(response) {
        $scope.testimonials = response.data;
    });
})
.controller('favoriteCourseController', function($scope, baseServices , $http){
	$http.get(baseServices.api_url + 'favorite_courses').then(function(response) {
        $scope.favorite_courses = response.data;
    });
})
.controller('openClassController', function($scope, baseServices , $http){
	$http.get(baseServices.api_url + 'open_classes').then(function(response) {
		// console.log(response.data);
        $scope.open_classes = response.data;
    });
    // $('#open-class > .course-features').matchHeight({
    // 	target: $('#favorite-course')
    // });
})
.controller('featureTeacherController', function($scope, baseServices , $http){
	$http.get(baseServices.api_url + 'feature_teachers').then(function(response) {
        $scope.feature_teachers = response.data;
        $scope.$on('ngRepeatFinished', function(ngRepeatFinishedEvent) {
        	$('#featured_teachers .teacher-img img').width($('#featured_teachers .teacher-img img').height());
		    
		    var minSlides=1;

		    if ($(window).width() > 768) {
			  	minSlides = 4;
			};

		    $('#feature-teacher-carousel .bxslider').bxSlider({
		    	slideWidth: 3000,
		    	auto:true,
		    	minSlides: minSlides,
		    	maxSlides: 4,
		    	pager: false,
		    });
		});
    });
})
