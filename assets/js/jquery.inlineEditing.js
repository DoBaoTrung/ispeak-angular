/**
 *
 * Custom jquery inline editing
 *
 */

 $.fn.inlineEditing = function(replaceWith) {

    $(this).hover(function() {
    	// alert(app.url);
        $(this).addClass('hover');
    }, function() {
        $(this).removeClass('hover');
    });

    $(this).click(function() {

        var elem = $(this);
        var model = $(this).attr('model');
        var field = $(this).attr('edit-field');
        var data_id = $(this).attr('data-id');
        

        elem.hide();
        elem.after(replaceWith);

        replaceWith.val($.trim(elem.text()));
        replaceWith.focus();

        replaceWith.blur(function() {
            save_inline_data();
        });

        replaceWith.keypress(function(e) {
            if(e.which == 13) {
                // enter pressed
                save_inline_data();
            }
        });

        function save_inline_data(){
            if (replaceWith.val() != $.trim(elem.text())) {
                // ajax update
                replaceWith.after('<span class="icon-saving"><span>');
                // elem.text($(this).val());
                // var note = $(this).val();
                var field_value = replaceWith.val();

                $.ajax({
                    url: app.base_url + 'base/template/save_inline_data/' + data_id,
                    type: 'POST',
                    dataType: 'json',
                    data: {
                        model: model,
                        field: field,
                        field_value: field_value,
                    },
                })
                .done(function(res) {
                    if (res.status == 1){
                        // Đổi text hiển thị
                        elem.text(field_value);
                        // alert(res.messages);
                    }else{
                        alert(res.messages);
                    }
                    
                    replaceWith.remove();
                    elem.show();
                    $('span.icon-saving').remove();
                });

            }else{
                replaceWith.remove();
                elem.show();
            }
        }

    });
};
