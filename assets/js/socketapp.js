var socket = null;

function userLogined(username){
    //console.log('user join');
    socket.emit('user logined', username, function (data) {
        // if (data) 
        // {
        //     console.log('Add tai khoan thanh cong.');
        // } else {
        //     console.log('Tai khoan da duoc su dung. Vui long dien tai khoan khac');
        // }
    });
}

function teacherJoinClass(bookingid, teacher, student, join_url) {
    socket.emit('teacher joined class', bookingid, teacher, student , join_url, function (data) {
        // if (data) 
        // {
        //     console.log('Thông báo joined class thành công!');
        // } else {
        //     console.log('Học viên chưa online');
        // }
    });
}

function teacherJoinOpenClass(bookingid, teacher, students, join_url) {
    socket.emit('teacher joined openclass', bookingid, teacher, students , join_url, function (data) {
        // if (data) 
        // {
        //     console.log('Thông báo joined class thành công!');
        // } else {
        //     console.log('Học viên chưa online');
        // }
    });
}

jQuery(document).ready(function($) {
    
    socket = io.connect('https://socket.ispeak.vn', {secure: true});

    socket.on('connect', function () {
        // socket connected
        userLogined(app.username);
        // console.log('success connect to server!');

    });

    socket.on('disconnect', function(){
        // console.log('disconnect!');
    });

    //usernameClick();

    socket.on('class opened', function (data) {

        //Cap nhat trang thai nut join
        $('.btn-join-class-' + data.bookingid).attr('href', data.join_url);
        $('.btn-join-class-' + data.bookingid).removeClass('disabled');

        $.toast({
            text: "Giáo viên của bạn đã vào lớp, bạn hãy vào <a href='/backend/student#lich-hoc'>lịch học</a> của bạn để nhấn nút vào lớp hoặc bấm <a class='label label-warning' style='padding: 3px 0;' target='_blank' href='" + data.join_url + "'>vào đây</a> để vào lớp nhanh!",
            hideAfter: false,
            icon: "error",
            stack: 8,
            position: 'bottom-left',
            classes: 'animated shake'
        });

        var joinedClassSound = new buzz.sound("/themes/ispeakv3/sounds/doorbell_sms.mp3");
        joinedClassSound.play();


    });

    socket.on('openclass opened', function (data){
        //Cap nhat trang thai nut join
        $('.btn-join-openclass-' + data.bookingid).attr('href', data.join_url);
        $('.btn-join-openclass-' + data.bookingid).removeClass('disabled');

        $.toast({
            text: "Giáo viên của bạn đã vào lớp, bạn hãy vào <a href='/backend/student#lich-hoc-mo'>lịch học</a> của bạn để nhấn nút vào lớp hoặc bấm <a class='label label-warning' style='padding: 3px 0;' target='_blank' href='" + data.join_url + "'>vào đây</a> để vào lớp ngay!",
            hideAfter: false,
            icon: "error",
            stack: 8,
            position: 'bottom-left',
            classes: 'animated shake'
        });

        var joinedClassSound = new buzz.sound("/themes/ispeakv3/sounds/doorbell_sms.mp3");
        joinedClassSound.play();
    });


    socket.on('usernames', function (data) {
        // console.log(data);
        $.ajax({
            url: app.base_url + 'frontend/chat/get_list_users',
            type: 'post',
            dataType: 'html',
            data: {
                usernames : data.toString(),
            },
            beforeSend: function(){
                //$('.btn-login .fa').removeClass('fa-clock').addClass('fa-spinner fa-pulse');
            },
            success: function(list_users){
                //console.log('res: ' + list_users);
                $('.ispeak-chat-box-warp .list-body').html(list_users);
                bindClick();
            }
        });

    });

    $('#txtmessage').keypress(function (e) {
        // e.preventDefault();
        if (e.keyCode == 13) {
            var msg = $(this).val();
            socket.emit('send message', msg, $('#to-user').val());
            $(this).val('');
        }
    });

    $('#enter-chat').click(function(event) {
        /* Act on the event */
        var msg = $('#txtmessage').val();
        socket.emit('send message', msg, $('#to-user').val());
        $('#txtmessage').val('');
    });

    socket.on('new message', function (data) {

        // console.log('MSG:' + data.nick + '=>' + data.sendto + ":" + data.msg);

        //Nếu là message của mình
        var classes = data.nick == app.username ? 'right' : 'left';

        var currentdate = new Date(); 
        var datetime = '<small class="time">' + currentdate.getHours() + ":"  + currentdate.getMinutes() + '</small>';
        var message = '<div class="chatbox-message '+ classes +'">' + data.msg + datetime + '</div>';

        //Kiểm tra để chắc chắn hơn người này phải là người gửi hoặc người nhận
        if ( app.username == data.nick || app.username == data.sendto ){

            //Chỉ add message khi user là mình hoặc người đang chat
            if ( app.username == data.nick || data.nick ==  $('#to-user').val() ){
                $('#ispeak-chat-box .chatbox-body').append(message);
                $('#ispeak-chat-box .chatbox-body').scrollTop($('#ispeak-chat-box .chatbox-body')[0].scrollHeight);
            }
            
            //Nếu không mở cửa sổ hoawch đang chát với user khác thì notify hiện ra
            if ( app.username != data.nick && (! $('#ispeak-chat-box .chatbox-icon').hasClass('active') // Không bật cửa sổ
                ||  ( $('#ispeak-chat-box .chatbox-icon').hasClass('active') && $('#to-user').val() != data.nick ) //Đang chát với user khác
                )){
                $.ajax({
                    url: app.base_url + 'frontend/chat/get_user_info',
                    type: 'post',
                    dataType: 'html',
                    data: {
                        fuser : data.nick,
                        msg : data.msg,
                    },
                    success: function(html){
                        //console.log(html);
                        $('#ispeak-chat-box .float-chatbox').html(html);
                        $('#ispeak-chat-box .float-chatbox').show();

                        bindClick();

                        //Play sound
                        var joinedClassSound = new buzz.sound("/themes/ispeakv3/sounds/doorknockm.mp3");
                        joinedClassSound.play();

                    },
                    error : function(){
                        console.log('error get userinfo');
                        return false;
                    }
                });
            }
        }else{
            return false;
        }
            
        //Lưu vào database chỉ lưu 1 lần thôi khi người nhận được msg
        if (app.username == data.sendto){
            $.ajax({
                url: app.base_url + 'frontend/chat/save_message',
                type: 'post',
                dataType: 'text',
                data: {
                    fuser : data.nick,
                    tuser : data.sendto,
                    msg : data.msg,
                },
                success: function(res){
                    // console.log(res);
                },
                error : function(){
                    console.log('error save message');
                    return false;
                }
            });
        }
    });

});


function bindClick()
{

    //Mở cửa sổ chatbox khi ấn vào user trong list
    $('#ispeak-chat-box .user-info, .float-chatbox').click(function(event) {
        /* Act on the event */
        var thisctr = $(this);

        if ($(this).hasClass('float-chatbox')){
            $(this).hide();
        }

        var fullname = $(this).find('.fullname').first().val();
        var username = $(this).find('.username').first().val();
        var avatar = $(this).find('.avatar').first().val();
        var group = $(this).find('.group').first().val();

        $('#ispeak-chat-box .chatbox .chatbox-header .username').text(fullname);
        $('#ispeak-chat-box .chatbox .chatbox-header .group').text(group);
        $('#ispeak-chat-box .chatbox .chatbox-header .user_avatar').attr('src', avatar);
        $('#ispeak-chat-box #to-user').val(username);

        $.ajax({
            url: app.base_url + 'frontend/chat/get_last_message',
            type: 'post',
            dataType: 'html',
            data: {
                myuser : app.username,
                username : username,
            },
            beforeSend: function(){
                //$('.btn-login .fa').removeClass('fa-clock').addClass('fa-spinner fa-pulse');
            },
            success: function(last_message){
                //console.log('res: ' + list_users);
                $('.ispeak-chat-box-warp .chatbox .chatbox-body').html(last_message);
                $('.ispeak-chat-box-warp').show();
                $('#ispeak-chat-box .list-chat').hide();
                $('#ispeak-chat-box .chatbox').show();

                if ( ! $('#ispeak-chat-box .chatbox-icon').hasClass('active')){
                    $('#ispeak-chat-box .chatbox-icon').addClass('active');
                }

            },
            error : function(){
                console.log('error get last message');
                return false;
            }
        });
    });



    $('#ispeak-chat-box .chatbox-header .fa').click(function(event) {
        /* Act on the event */
        $('#ispeak-chat-box .chatbox').hide();
        $('#ispeak-chat-box .list-chat').show();
    });
}

