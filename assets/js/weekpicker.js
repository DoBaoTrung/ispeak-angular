$(function() {
    var startDate;
    var endDate;
    
    var selectCurrentWeek = function() {
        window.setTimeout(function () {
            $('#weekpicker').datepicker('widget').find('.ui-datepicker-current-day a').addClass('ui-state-active')
        }, 1);
    }
    
    $('#weekpicker').datepicker( {
        dateFormat: "dd/mm/yy",
        firstDay: 1,
        showOtherMonths: true,
        selectOtherMonths: true,
        showWeek: true,
        onSelect: function(dateText, inst) { 
            var date = $(this).datepicker('getDate');
            startDate = new Date(date.getFullYear(), date.getMonth(), date.getDate() - date.getDay() + 1);
            endDate = new Date(date.getFullYear(), date.getMonth(), date.getDate() - date.getDay() + 7);
            var dateFormat = inst.settings.dateFormat || $.datepicker._defaults.dateFormat;
            $('#weekpicker').val($.datepicker.formatDate( dateFormat, startDate, inst.settings )
                 + ' - ' + $.datepicker.formatDate( dateFormat, endDate, inst.settings ));
            
            selectCurrentWeek();

            //alert($('#weekpicker').val());
            $('#weekpicker').trigger('change');
        },
        beforeShow: function() {
            selectCurrentWeek();
            setTimeout("applyWeeklyHighlight()", 100);
        },
        beforeShowDay: function(date) {
            var cssClass = '';
            if(date >= startDate && date <= endDate)
                cssClass = 'ui-datepicker-current-day';
            return [true, cssClass];
        },
        onChangeMonthYear: function(year, month, inst) {
            selectCurrentWeek();
            setTimeout("applyWeeklyHighlight()", 100);
        }
    }).keyup(function() { setTimeout("applyWeeklyHighlight()", 100); });
    
   
});


function applyWeeklyHighlight()
{

    $('.ui-datepicker-calendar tr').each(function() {

        if($(this).parent().get(0).tagName == 'TBODY')
        {
            $(this).mouseover(function() {
                    $(this).find('a').css({'background':'#ffffcc','border':'1px solid #dddddd'});
                    $(this).find('a').removeClass('ui-state-default');
                    $(this).css('background', '#ffffcc');
            });
            $(this).mouseout(function() {
                    $(this).css('background', '#ffffff');
                    $(this).find('a').css('background','');
                    $(this).find('a').addClass('ui-state-default');
            });
        }

    });
}