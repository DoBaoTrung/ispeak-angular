// update unit when course change
//=================================
function update_units(){
    //hiện course type tương ứng
    var course_id = $("#form_course_id").val();
    $.ajax({
        url: app.ajax_get_unit_by_course + '/' + course_id,
        type: 'GET',
        dataType: 'html',
        beforeSend: function(){
            $('#form_unit_id').addClass('input-loading');
        },
        success: function(data){
            $('#form_unit_id').html(data).removeClass('input-loading');
        }
    });
    update_unit_price(course_id);
}

// Cập nhật giá buổi học của course
function update_unit_price(course_id){
    var teacher_id = $('#teacher_id').val();
    $.ajax({
        url: app.ajax_get_unit_price + course_id + '/' + teacher_id,
        type: 'GET',
        dataType: 'json',
        beforeSend: function(){
            $('#unit_price').addClass('input-loading');
        },
        success: function(json){
            //var price_ratio = parseFloat($('#teacher_price_ratio').val());
            $('#unit_price').text($.number(parseFloat(json.unit))).removeClass('input-loading');
            $('#booking_price').val(parseInt(json.unit));
        }
    });
}


$(document).ready(function(){
	// Booking 

    $('#form_course_id').change(function(event) {
        update_units();
    });

    //Giao vien dat lich
    $('.dat-lich-day .img-lich').click(function(){
        // alert('ok');
        var inputhidden = $(this).find('input[type=hidden]').first();
        var lich = $(this);
        $.ajax({
            url: app.ajaxSetCalendar,
            type: 'POST',
            data: {
                time: inputhidden.attr('time'),
                endtime: inputhidden.attr('endtime'),
                teacher: inputhidden.attr('teacher'),
            },
            dataType: "json",
            beforeSend: function(){
                lich.addClass('input-loading');
            },
            success: (function(data){
                // alert(data.message);
                lich.removeClass('input-loading');

                if (data.ok == 1){
                    if (data.set == 1) {
                        lich.addClass('open');
                        inputhidden.val(1);

                        $.toast({
                            text: 'You have successfully book your teach schedule',
                            hideAfter: 2000,
                            icon: 'success',
                            stack: 4,
                            position: 'top-right',
                        });
                    }else{
                        lich.removeClass('open');
                        inputhidden.val(0);
                        $.toast({
                            text: 'You have successfully cancel book your teach schedule',
                            hideAfter: 2000,
                            icon: 'success',
                            stack: 4,
                            position: 'top-right',
                        });
                    }
                }else{
                    alert(data.message);
                    $.toast({
                        text: data.message,
                        hideAfter: 2000,
                        icon: 'error',
                        stack: 4,
                        position: 'top-right',
                    });
                }
            })
        });
    });

    $('.btn_request_calendar').click(function(){

        var logined = $(this).attr('logined');
        if (logined == 'false'){
            alert('Bạn phải đăng nhập trước khi gửi yêu cầu lịch học!');
            return false;
        }

        $('#form_request_calendar .teacher').text($(this).attr('teacher-name'));
        $('#form_request_calendar #teacher_id').val($(this).attr('teacher-id'));

        $('#form_request_calendar').popup('show');
    });	


    $('#weekpicker').change(function(event) {
        /* Act on the event */
        var week = $(this).val();
        $.ajax({
            url: app.base_url + 'api/getweekdays/',
            type: 'POST',
            dataType: 'html',
            data: {
                selectWeek : week,
            },
            beforeSend: function(){
                $('#selectDate').addClass('input-loading');
            },
            success: function(res){
                $('#selectDate').html(res);//.trigger('change');
                $('#selectDate').removeClass('input-loading');
            }
        });

    });

    // $('#selectDate').change(function(event) {
    //     /* Act on the event */
    //     $('#form_chang_week').prop('disabled', 'true');
    //     $( location ).attr("href", app.base_url + 'backend/dat-lich-hoc/' + $(this).val());
    // });

    $('#form_get_calendars').click(function(event) {
        /* Act on the event */
        event.preventDefault();

        var date = $('#selectDate').val();
        var location_id = $('#location_id').val();

        if ( location_id > 0 ){
            $( location ).attr("href", app.base_url + 'backend/dat-lich-hoc/' + date + '/' + location_id);
        }else{
            $( location ).attr("href", app.base_url + 'backend/dat-lich-hoc/' + date );
        }

    });

    // Học viên đặt lịch
    $('.booking.img-lich.open').click(function(event){
        event.preventDefault();
        // Gán giá trị trước khi mở form
        $('#form_booking .teacher-name').text($(this).attr('teacher-name'));
        $('#teacher_id').val($(this).attr('teacher-id'));
        $('#form_booking .time').text($(this).attr('booking-time-string'));
        $('#form_booking #calendar_id').val($(this).attr('calendar-id'));
        $('#form_booking #teacher_price_ratio').val($(this).attr('teachr-price-ratio'));

        $('#booking-status').removeClass('alert-success alert-danger').html('').hide();
        $('.btn-close-booking').hide();
        $('.btn-save-booking').show();
        
        $('#form_booking').popup('show');

        update_units();

    });

    $('.btn-save-booking').click(function(event) {
    	/* Act on the event */
    	event.preventDefault();

    	var thisbtn = $(this);

    	$.ajax({
            url: app.base_url + 'backend/booking/save_booking',
            type: 'POST',
            dataType: 'json',
            data: {
                student_id : $('#form_student_id').val(),
                teacher_id : $('#teacher_id').val(),
                calendar_id: $('#calendar_id').val(),
                package_id : $('#form_package_id').val(),
                course_id  : $('#form_course_id').val(),
                unit_id    : $('#form_unit_id').val(),
                price      : $('#booking_price').val(),
                is_use_zoom: $('#form_is_use_zoom').val(),
                note       : $('#note').val(),
            },
            beforeSend: function(){
                thisbtn.find('.fa').removeClass('fa-calendar-plus-o').addClass('fa-spinner fa-pulse');
                thisbtn.addClass('disabled');
            },
            success: function(res){

                thisbtn.find('.fa').removeClass('fa-spinner fa-pulse').addClass('fa-calendar-plus-o');

                $('#booking-status').html(res.msg);

                if (res.status == 1)
                {
                	thisbtn.fadeOut();
                    
                	$('#booking-status').addClass('alert-success');
                	$('.btn-close-booking').fadeIn();

                	//change calandar status 
                	$( '#calendar_' + $('#calendar_id').val() ).removeClass('open form_booking_open').addClass('booked');

                    setTimeout(function(){
                    	$('#form_booking').popup('hide');
                    }, 3000);

                    //Emmit socket
                    
                }else{
                	$('#booking-status').addClass('alert-danger');
                }

                thisbtn.removeClass('disabled');

                $('#booking-status').fadeIn();
            }

        });//end ajax

    });//end function



});