$(document).ready(function() {

	$('.send_booking_report').click(function(event) {
		/* Act on the event */
		event.preventDefault();

		$('#report_booking_id').val($(this).data('id'));

		$('#report-status').text('').fadeOut();

		$('.btn-send-report').show();

		$('#form_booking_report').popup('show');
	});

    $('.btn-send-report').click(function(event) {
    	/* Act on the event */
    	event.preventDefault();

    	var thisbtn = $(this);

    	$.ajax({
            url: app.base_url + 'backend/booking/save_booking_report',
            type: 'POST',
            dataType: 'json',
            data: {
                booking_id : $('#report_booking_id').val(),
                report_id : $('#form_report_id').val(),
                note: $('#form_report_note').val(),
            },
            beforeSend: function(){
                thisbtn.find('.fa').removeClass('fa-calendar-plus-o').addClass('fa-spinner fa-pulse');
                thisbtn.addClass('disabled');
            },
            success: function(res){

                thisbtn.find('.fa').removeClass('fa-spinner fa-pulse').addClass('fa-calendar-plus-o');

                $('#report-status').text(res.msg);

                if (res.status == 1)
                {
                	thisbtn.fadeOut();
                    
                	$('#report-status').addClass('alert-success');

                    setTimeout(function(){
                    	$('#form_booking_report').popup('hide');
                    }, 5000);
                    //Emmit socket
                }else{
                	$('#report-status').addClass('alert-danger');
                }

                thisbtn.removeClass('disabled');

                $('#report-status').fadeIn();
            },
            error: function() {
            	/* Act on the event */
            	thisbtn.find('.fa').removeClass('fa-spinner fa-pulse').addClass('fa-calendar-plus-o');
            	thisbtn.removeClass('disabled');
            }

        });//end ajax

    });//end function

    


});