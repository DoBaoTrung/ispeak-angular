function change_date_format(idate){
    // var res = idate.split("-");
    // return res[2] + '-' + res[1] + '-'+ res[0];
    var dateObj = new Date(idate);
    var month = dateObj.getMonth() + 1;
    var day = dateObj.getDate();
    if (month < 10){
        month = '0' + month;
    } 
    if (day < 10){
        day = '0' + day;
    }
    return dateObj.getFullYear() + '-' + month + '-' + day;
}

$(document).ready(function() {
    $('#send_inquiry').click(function(event) {
        /* Act on the event */
        event.preventDefault();

        var thisbtn = $(this);
        var name = $('#inquiry_name').val();
        var phone = $('#inquiry_phone').val();
        var email = $('#inquiry_email').val();

        if ( email == ''){
            $('#inquiry_email').focus();
            $('#inquiry_status').text('Vui lòng nhập email liên hệ..!');
        }
        if ( phone == ''){
            $('#inquiry_phone').focus();
            $('#inquiry_status').text('Vui lòng nhập số điện thoại liên hệ..!');
        }
        if ( name == ''){
            $('#inquiry_name').focus();
            $('#inquiry_status').text('Vui lòng nhập tên liên hệ..!');
        }

        if ( name == '' || phone == '' || email == ''){
            $('#inquiry_status').addClass('alert alert-danger').show();
            return false;
        }

        $.ajax({
            url: app.ajax_send_enquiry,
            type: 'POST',
            dataType: 'json',
            data: {
                name: name,
                phone: phone,
                email: email,
            },
            beforeSend: function(){
                thisbtn.prop('disabled', true).addClass('input-loading');
                $('#inquiry_status').removeClass('alert alert-danger alert-success').hide();
            },
            success: function(json)
            {
                thisbtn.removeClass('input-loading');

                if (json.status == 1)
                {
                    $('#inquiry_status').addClass('alert alert-success');
                }
                else
                {
                    $('#inquiry_status').addClass('alert alert-danger');

                    thisbtn.removeAttr('disabled');
                }

                $('#inquiry_status').text(json.msg).show();
            }
        });
    });
});

$(document).ready(function() {

    $('.bxslider').bxSlider({
        responsive: true,
        adaptiveHeight: true
    });

    $("select.custom").each(function() {                    
        var sb = new SelectBox({
            selectbox: $(this),
            height: 150,
            width: 250
        });
    });

    //js chart circle
    $('.percent-circle').each(function(index, el) {

        var percent = $(el).attr('data-percent');
        // alert(percent);

        $(el).circliful({
            animation: 1,
            animationStep: 1,
            foregroundBorderWidth: 15,
            backgroundBorderWidth: 15,
            percent: percent,
            textSize: 28,
            textStyle: 'font-size: 14px;',
            textColor: '#222',
            multiPercentage: 1,
            percentages: [10, 20, 30]
        });
    });

    var replaceWith = $('<input name="temp" type="text" class="temp"/>');
    $('.inlineEditor').inlineEditing(replaceWith);

	
	$('.select2').chosen();

    $('.datatable').dataTable();

	$( "#form_from_date" ).datepicker({
      defaultDate: "+1w",
      changeMonth: true,
      showWeek: true,
      dateFormat: "dd/mm/yy",
      numberOfMonths: 1,
      onClose: function( selectedDate ) {
        $( "#form_to_date" ).datepicker( "option", "minDate", selectedDate );
      }
    });

    $( "#form_to_date" ).datepicker({
        defaultDate: "+1w",
        changeMonth: true,
        showWeek: true,
        dateFormat: "dd/mm/yy",
        numberOfMonths: 1,
        onClose: function( selectedDate ) {
            $( "#form_from_date" ).datepicker( "option", "maxDate", selectedDate );
        }
    });


	$('.mypopup').popup({
	  	transition: 'all 0.3s'
	});

    //bat banner popup

    if ( $('#home_popup_banner').length > 0)
    {
        setTimeout(function(){
            // location.reload();
            $('#home_popup_banner').show();
            $('#home_popup_banner').popup('show');
        }, 5000);
    }
	

	$('.summernote').summernote({
        height: 200
    });

	$(".main-banner").owlCarousel({
		navigation : false,
		slideSpeed : 300,
		paginationSpeed : 400,
		singleItem:true
	});

	var owl = $(".slider-tc");
	owl.owlCarousel({
		itemsCustom : [
		[0, 2],
		[450, 3],
		[700, 4],
		[1000, 5]
		],
		pagination : false,
		navigation : true
	});

	$(".main-comment").owlCarousel({
		navigation : false,
		slideSpeed : 300,
		paginationSpeed : 400,
		singleItem:true
	});

    $(".flip").click(function(){
        // alert('text');
        $(this).find(".panel").slideToggle("slow");
    });

    $('.fancybox').fancybox({
        width: "100%",
        padding : 0,
        // openEffect  : 'elastic',
        helpers : {
            title : null            
        }  
    });


	try {
		if ($(".animated")[0]) {
			$('.animated').css('opacity', '0');
		}

		$('.triggerAnimation').waypoint(function() {
			var animation = $(this).attr('data-animate');
			$(this).css('opacity', '');
			$(this).addClass("animated " + animation);

		},
			{
				offset: '80%',
				triggerOnce: true
			}
		);
		// menu_hover_effect
	    $("#nav_effect").append("<li id='magic-line'></li>");
	    var $magicLine = $("#magic-line");
	    $magicLine
	        .width($(".current_page_item").width())
	        .css("left", $(".current_page_item a").position().left)
	        .data("origLeft", $magicLine.position().left)
	        .data("origWidth", $magicLine.width());
	    $("#nav_effect li").find("a").hover(function() {
	        $el = $(this);
	        leftPos = $el.position().left;
	        newWidth = $el.parent().width();
	        $magicLine.stop().animate({
	            left: leftPos,
	            width: newWidth
	        });
	    }, function() {
	        $magicLine.stop().animate({
	            left: $magicLine.data("origLeft"),
	            width: $magicLine.data("origWidth")
	        });
	    });
		
	} catch(err) {

	}


    $('.row-eq-height').equalizer({
        breakpoint : 500,       // if browser less than this width, disable the plugin
        columns    : '.col-eq-height',
        disabled   : 'noresize' // class applied when browser width is less than the breakpoint value
    });

    $('.birthday, .datepicker').datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: "dd/mm/yy",
    });


    moveProgressBar();
    $(window).resize(function() {
        //moveProgressBar();
    });

    function moveProgressBar() {
        console.log("moveProgressBar");
        // level
        var getPercent = ($('.wrap-lv').data('progress-percent') / 100);
        var getProgressWrapWidth = $('.wrap-lv').width();
        var progressTotal = getPercent * getProgressWrapWidth;
        var animationLength = 2500;
        $('.bar-lv').stop().animate({
            left: progressTotal
        }, animationLength);

        // Vocabulary
        var getPercent = ($('.wrap-vb').data('progress-percent') / 100);
        var getProgressWrapWidth = $('.wrap-vb').width();
        var progressTotal = getPercent * getProgressWrapWidth;
        var animationLength = 2500;
        $('.bar-vb').stop().animate({
            left: progressTotal
        }, animationLength);

        // Reading
        var getPercent = ($('.wrap-rd').data('progress-percent') / 100);
        var getProgressWrapWidth = $('.wrap-rd').width();
        var progressTotal = getPercent * getProgressWrapWidth;
        var animationLength = 2500;
        $('.bar-rd').stop().animate({
            left: progressTotal
        }, animationLength);

        // Grammar
        var getPercent = ($('.wrap-gm').data('progress-percent') / 100);
        var getProgressWrapWidth = $('.wrap-gm').width();
        var progressTotal = getPercent * getProgressWrapWidth;
        var animationLength = 2500;
        $('.bar-gm').stop().animate({
            left: progressTotal
        }, animationLength);

        // Speaking
        var getPercent = ($('.wrap-sk').data('progress-percent') / 100);
        var getProgressWrapWidth = $('.wrap-sk').width();
        var progressTotal = getPercent * getProgressWrapWidth;
        var animationLength = 2500;
        $('.bar-sk').stop().animate({
            left: progressTotal
        }, animationLength);

        // Listening
        var getPercent = ($('.wrap-lt').data('progress-percent') / 100);
        var getProgressWrapWidth = $('.wrap-lt').width();
        var progressTotal = getPercent * getProgressWrapWidth;
        var animationLength = 2500;
        $('.bar-lt').stop().animate({
            left: progressTotal
        }, animationLength);
    }

});


$('.toast').each(function(index, o) {
     /* iterate through array or object */
    $.toast({
        text: $(o).html(),
        hideAfter: 300000,
        icon: $(o).attr('toast-ico'),
        stack: 8,
        position: 'bottom-left',
    });
});

/**
 * SIDEBAR MULTILEVEL MENU
 */

$(".cc-menu > ul > li").each(function(index, el) {
    if ($(this).find('ul').length>0){
        $(this).addClass('has-submenu');
        $(this).prepend('<span class="toggle"><i class="fa fa-caret-down"></i></span>');
    }
    if ($(this).find('.current').length>0){
        $(this).addClass('active');
        $(this).find('ul:first').slideDown(400);
        $(this).find('.toggle').find('.fa').removeClass('fa-caret-down').addClass('fa-caret-up');
    }
});

$(".cc-menu > ul > li.has-submenu > .toggle").click(function(e) {
    if ($(this).closest('li').hasClass('active')){
        $(this).closest('li').removeClass('active');
        $(this).siblings('ul').slideUp(400);
        $(this).find('.fa').removeClass('fa-caret-up').addClass('fa-caret-down');
    }
    else {
        $(this).closest('li').addClass('active');
        $(this).siblings('ul').slideDown(400);
        $(this).find('.fa').removeClass('fa-caret-down').addClass('fa-caret-up');
    }
    
    
    // $(this).find('.fa').toggleClass('fa-caret-down fa-caret-up');
    
});

// $(".cc-menu > ul > li.active.has-submenu > .toggle").click(function(e) {
//     $(this).closest('li').removeClass('active');
//     $(this).siblings('ul').slideUp('400');
     // $(this).find('.fa').removeClass('fa-caret-up').addClass('fa-caret-down');
// });