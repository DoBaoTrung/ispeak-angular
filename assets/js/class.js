$(document).ready(function() {
	//Lựa chọn khoá học
    $('.toggle_course').click(function(event) {
        /* Act on the event */
        event.preventDefault();
        
        var toggleTo = $(this).attr('toggleClass');
        $(toggleTo).toggleClass('hidden');
    });


    $('.btn-open-or-join-class').click(function(event) {
        /* Act on the event */
        event.preventDefault();

        var bookingId = $(this).attr('booking');

        var thisbtn = $(this);

        thisbtn.prop('disable', true);

        //alert('Join class: ' + bookingId);

        $.ajax({
            url: app.base_url + 'backend/room/open_or_join/' + bookingId,
            type: 'get',
            dataType: 'json',
            beforeSend: function(){
                thisbtn.find('.fa').removeClass('fa-sign-in').addClass('fa-spinner fa-pulse');
            },
            success: function(data){
                thisbtn.find('.fa').removeClass('fa-spinner fa-pulse').addClass('fa-sign-in');

                if (data.status == 0){
                    alert(data.msg);
                }else{
                    //truyen link vao #hidden-link và trigger click
                    $('#zoom-window').attr('src', data.start_url);
                    $('.zoom-window-wrap_open').trigger('click');

                    //Gửi socjket Giáo viên join class
                    teacherJoinClass(bookingId, data.teacher, data.student, data.join_url);
                    
                }
            }
        });

    });

    //Join openclass
    $('.btn-start-openclass').click(function(event) {
        /* Act on the event */
        event.preventDefault();

        var bookingId = $(this).data('id');

        var thisbtn = $(this);

        thisbtn.prop('disable', true);

        //alert('Join class: ' + bookingId);

        $.ajax({
            url: app.base_url + 'backend/openclass/open_or_join/' + bookingId,
            type: 'get',
            dataType: 'json',
            beforeSend: function(){
                thisbtn.find('.fa').removeClass('fa-sign-in').addClass('fa-spinner fa-pulse');
            },
            success: function(data){
                thisbtn.find('.fa').removeClass('fa-spinner fa-pulse').addClass('fa-sign-in');

                if (data.status == 0){
                    alert(data.msg);
                }else{
                    //truyen link vao #hidden-link và trigger click
                    $('#zoom-window').attr('src', data.start_url);
                    $('.zoom-window-wrap_open').trigger('click');
                    console.log(data.students);
                    //Gửi socjket Giáo viên join class
                    teacherJoinOpenClass(bookingId, data.teacher, data.students, data.join_url);
                }
            },
            error: function(){
                thisbtn.find('.fa').removeClass('fa-spinner fa-pulse').addClass('fa-sign-in');
            }
        });

    });

    //  Student rate teacher
    $('.rate_teacher').click(function(){
        //alert('review');
        $('#form_review .teacher').text($(this).attr('teacher'));
        $('#form_review #booking_id').val($(this).attr('booking'));
        $('#form_review').popup('show');
    });

    // Teacher set student start level
    $('.set_start_level').click(function(){
        //alert('review');
        $('#form_set_start_level .student').text($(this).attr('student'));
        $('#form_set_start_level #student-id').val($(this).attr('student-id'));
        $('#form_set_start_level').popup('show');
    });


    // Học viên làm bài tập về nhà
    $('.student_do_homework').click(function(event){
        event.preventDefault();
        //alert('review');
        $('#booking_id').val($(this).attr('booking'));
        $('#booked_date').text($(this).attr('booked-date'));
        $('#course_title').text($(this).attr('course'));
        $('#teacher_name').text($(this).attr('teacher'));
        $('#unit_name').text($(this).attr('unit'));
        $('#vocabulary').text($(this).attr('vocabulary'));
        $('#writing').text($(this).attr('writing'));
        $('#vocabulary_answer').text($(this).attr('vocabulary-answer'));
        $('#writing_answer').text($(this).attr('writing-answer'));
        $('#speaking').text($(this).attr('speaking'));

        $('#form_student_do_homework').popup('show');
    });

	// write memo
	$('.review_student').click(function(event){
        
        event.preventDefault();
        //alert('review');
        $('#form_review_student .student_name').text($(this).attr('student'));
        $('#form_review_student #booking_id').val($(this).attr('booking'));
        $('#form_review_student .booked-date').text($(this).attr('booked-date'));
        $('#form_review_student .course-title').text($(this).attr('course-title'));
        $('#form_review_student .unit').text($(this).attr('unit'));

        $('#form_review_student').popup('show');
        
    });

    $('.btn-memo-preview').click(function(event) {
        /* Act on the event */
        event.preventDefault();
        $('#form_review_student input[name!="review_student"], #form_review_student select').prop( "readonly", true );

        $(this).addClass('hidden');

        $('.btn-memo-edit-mode, .btn-memo-send').removeClass('hidden');

    });

    $('.btn-memo-edit-mode').click(function(event) {
        /* Act on the event */
        event.preventDefault();
        $('#form_review_student input[name!="review_student"], #form_review_student select').prop( "readonly", false );

        $(this).addClass('hidden');
        
        $('.btn-memo-send').addClass('hidden');

        $('.btn-memo-preview').removeClass('hidden');
    });


    $('.btn-memo-send').click(function(event) {
    	/* Act on the event */
    	event.preventDefault();
    	var thisbtn = $(this);

    	$.ajax({
            url: app.base_url + 'backend/booking/save_teacher_memo/',
            type: 'post',
            dataType: 'json',
            data: {
            	booking_id : $('#booking_id').val(),
            	vocabulary_point : $('#vocabulary_point').val(),
            	grammar_point : $('#grammar_point').val(),
            	speaking_point : $('#speaking_point').val(),
            	listening_point : $('#listening_point').val(),
            	reading_point : $('#reading_point').val(),
            	page_finished : $('#page_finished').val(),
            	strengths : $('#strengths').val(),
            	weaknesses : $('#weaknesses').val(),
            	teacher_note : $('#teacher_note').val(),
            	homework_vocabulary : $('#homework_vocabulary').val(),
            	homework_writing : $('#homework_writing').val(),
            	homework_speaking : $('#homework_speaking').val(),
            	next_lession : $('#next_lession').val(),
            	badge_id : $('#badge_id').val()
            },
            beforeSend: function(){
            	$('#form_review_student').find('.btn').addClass('disabled');
                thisbtn.find('.fa').removeClass('fa-save').addClass('fa-spinner fa-pulse');
                $('#status').removeClass('alert-success alert-danger').hide();
            },
            success: function(data)
            {
            	
                if (data.status == 1){

                	thisbtn.hide();

                	//Xoá dòng memo đã viết đi
                	$('#unwritten_memo_' + $('#booking_id').val()).fadeOut().remove();

                	$('#status').addClass('alert-success');

                    setTimeout(function(){
                    	$('#form_review_student').popup('hide');
                    }, 5000);
                }else{
                	$('#status').addClass('alert-danger');
                	thisbtn.find('.fa').addClass('fa-save');
                }

                $('#status').html(data.msg).show();
                $('#form_review_student').find('.btn').removeClass('disabled');
                thisbtn.find('.fa').removeClass('fa-spinner fa-pulse');
                
                //Gửi socjket Giáo viên viết memo
                //teacherJoinClass(bookingId, data.teacher, data.student, data.join_url);
            }
        });

    });
    
});