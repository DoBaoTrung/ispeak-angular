$(document).ready(function() {
	$('.btn-login').click(function(event) {
        /* Act on the event */
        event.preventDefault();
        var username = $('#username').val();
        var password = $('#password').val();

        $.ajax({
            url: app.ajax_login_url,
            type: 'post',
            dataType: 'json',
            data: {
                username: $.trim(username),
                password: $.trim(password),
            },
            beforeSend: function(){
                $('#login-status').removeClass('alert alert-success alert-danger').html('');
                $('.btn-login .fa').removeClass('fa-clock').addClass('fa-spinner fa-pulse');
            },
            success: function(data){
                $('#login-status').html(data.message);
                if (data.status == 1){
                    $('#login-status').addClass('alert alert-success');

                    //alert(app.return_url);
                    
                    setTimeout(function(){
                        // location.reload();
                        if ( app.return_url != ''){
                            $( location ).attr('href', app.return_url);
                        }else{
                            $( location ).attr('href', data.redirect);
                        }

                    }, 500);
                }else{
                    $('#login-status').addClass('alert alert-danger');
                    $('.btn-login .fa').removeClass('fa-spinner fa-pulse').addClass('fa-clock');
                }
            }
        });
    });
});